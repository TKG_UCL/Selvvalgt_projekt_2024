**Dette mine egne noter og fremgangsmetode, jeg bygger promært på tryhackme rum for at holde en god struktur har jeg indsætter en kommentar til mig selv samt laver headers så der bliver lavet en indholdesfortegelse i egen note**
# Snort the Basics - Exeamples

--------------------
/Billeder/Uge10-snort/

Task 2 Writing IDS Rules (HTTP) 
--------------------------------

### What is the number of detected packets?

**Rule:** 

```text-plain
alert tcp any 80 <> any any (msg:"TCp inbound" sid:10000001; rev:1;)
alert tcp any any <> any 80 (msg:"TCp outbound" sid:10000002; rev:2;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l .  
```

![1](/Billeder/Uge10-snort/33_Snort the Basics - Exeamples_i.png)

#### Side track men hvad er forskellen?  
 

![2](/Billeder/Uge10-snort/38_Snort the Basics - Exeamples_i.png)

### What is the destination address of packet 63?

**Rule:** 

```text-plain
None - Kigge ri en log fil. 
```

**Comand:**  
 

```text-plain
sudo snort -r <Loktion på log fil> -n 63 
```

![3](/Billeder/Uge10-snort/11_Snort the Basics - Exeamples_i.png)

![4](/Billeder/Uge10-snort/4_Snort the Basics - Exeamples_i.png)

### What is the ACK number of packet 64?

**Comand:**  
 

```text-plain
sudo snort -r <Loktion på log fil> -n 64 
```

![5](/Billeder/Uge10-snort/30_Snort the Basics - Exeamples_i.png)

### What is the SEQ number of packet 62?

**Comand:**  
 

```text-plain
sudo snort -r <Loktion på log fil> -n 64 
```

![6](/Billeder/Uge10-snort/13_Snort the Basics - Exeamples_i.png)

###  What is the TTL of packet 65? | What is the source IP of packet 65?  | What is the source port of packet 65?

**Comand:**  
 

```text-plain
sudo snort -r <Loktion på log fil> -n 65 
```

![7](/Billeder/Uge10-snort/43_Snort the Basics - Exeamples_i.png)

Task 3 Writing IDS Rules (FTP) 
-------------------------------

### What is the number of detected packets?

**Rule:** 

```text-plain
alert tcp any 21 <> any any (msg:"FTP inbound" sid:10000001; rev:1;)
alert tcp any any <> any 21 (msg:"FTPoutbound" sid:10000002; rev:2;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l .  
```

![8](/Billeder/Uge10-snort/Snort the Basics - Exeamples_i.png)

### What is the FTP service name?

![9](/Billeder/Uge10-snort/46_Snort the Basics - Exeamples_i.png)

**Comand:**  
 

```text-plain
sudo snort -r <pcap> -X -n 10 
```

\-X gir en hele information i header:  
dette er uden -X. -d ville også kunne ha løst problemet.

![10](/Billeder/Uge10-snort/51_Snort the Basics - Exeamples_i.png)

### What is the number of detected packets?

**Rule:** 

```text-plain
alert tcp any 21 <> any any (msg:"FTP failed logins" sid:10000003; rev:3;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l . 

sudo snort -r <logs file>
```

![11](/Billeder/Uge10-snort/1_Snort the Basics - Exeamples_i.png)

Min regel er korrekt jeg kan bare ikke få snort til at genskabe en Log.123123 fil til mig hvilke gør jeg ikke kan forsætte dette rum så jeg har opsøgt de sidste svar… min udfordring er når jeg sletter “Alert filen og log.12312938” så gider snort ikke gengive mig nogle nye logs…..   
  
 

![12](/Billeder/Uge10-snort/18_Snort the Basics - Exeamples_i.png)

Task 4 Writing IDS Rules (PNG) 
-------------------------------

### Investigate the logs and identify the software name embedded in the packet

[https://en.wikipedia.org/wiki/List\_of\_file\_signatures](https://en.wikipedia.org/wiki/List_of_file_signatures)   UTROLIGT VIGTIGT UD fra denne hjemmeside er der en signatur liste som man kan bruge til at filtere efter givne pakker fx billeder som inde holder: `89 50 4E 47 0D 0A 1A 0A, detter primært i HEX =)` 

```text-plain
alert ICMP any 21 <> any any (msg:"FTP failed logins";content:"|89 50 4E 47 0D 0A 1A 0A|" sid:10000003; rev:3;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l . 

sudo snort -r <logs file>
```

![13](/Billeder/Uge10-snort/23_Snort the Basics - Exeamples_i.png)

![14](/Billeder/Uge10-snort/47_Snort the Basics - Exeamples_i.png)

### Investigate the logs and identify the image format embedded in the packet.

```text-plain
alert tcp any any  <> any any (msg:"FTP failed logins";content:"|47 49 46 38 39 61|" sid:10000003; rev:3;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l . 

sudo snort -r <logs file>
```

![15](/Billeder/Uge10-snort/5_Snort the Basics - Exeamples_i.png)

![16](/Billeder/Uge10-snort/27_Snort the Basics - Exeamples_i.png)

Task 5 Writing IDS Rules (Torrent Metafile) 
--------------------------------------------

### What is the number of detected packets?

```text-plain
alert tcp any any  <> any any (msg:"FTP failed logins";content:".torrent" sid:10000001; rev:1;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l . 

sudo snort -r <logs file>
```

![17](/Billeder/Uge10-snort/37_Snort the Basics - Exeamples_i.png)

![18](/Billeder/Uge10-snort/9_Snort the Basics - Exeamples_i.png)

### What is the name of the torrent application?

```text-plain
alert tcp any any  <> any any (msg:"FTP failed logins";content:".torrent" sid:10000001; rev:1;)
```

**Comand:**  
 

```text-plain

sudo snort -r <logs file> -X
```

![19](/Billeder/Uge10-snort/2_Snort the Basics - Exeamples_i.png)

![20](/Billeder/Uge10-snort/10_Snort the Basics - Exeamples_i.png)

Jeg mener dette er en fejl og det skulle  ha heddet **x-bittorrent…**

### What is the MIME (Multipurpose Internet Mail Extensions) type of the torrent metafile?

Jeg ved ikke hvad MIME er det noget jeg skal ha læst op på =) 

![21](/Billeder/Uge10-snort/16_Snort the Basics - Exeamples_i.png)

![22](/Billeder/Uge10-snort/40_Snort the Basics - Exeamples_i.png)

![23](/Billeder/Uge10-snort/42_Snort the Basics - Exeamples_i.png)

### What is the hostname of the torrent metafile?

![24](/Billeder/Uge10-snort/41_Snort the Basics - Exeamples_i.png)

![25](/Billeder/Uge10-snort/36_Snort the Basics - Exeamples_i.png)

Task 6 Troubleshooting Rule Syntax Errors 
------------------------------------------

### What is the number of the detected packets?

![26](/Billeder/Uge10-snort/29_Snort the Basics - Exeamples_i.png)

![27](/Billeder/Uge10-snort/39_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

![28](/Billeder/Uge10-snort/3_Snort the Basics - Exeamples_i.png)

![29](/Billeder/Uge10-snort/35_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

![30](/Billeder/Uge10-snort/45_Snort the Basics - Exeamples_i.png)

![31](/Billeder/Uge10-snort/44_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

![32](/Billeder/Uge10-snort/48_Snort the Basics - Exeamples_i.png)

![33](/Billeder/Uge10-snort/22_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

SAmme som før

![34](/Billeder/Uge10-snort/24_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

![35](/Billeder/Uge10-snort/6_Snort the Basics - Exeamples_i.png)

![36](/Billeder/Uge10-snort/28_Snort the Basics - Exeamples_i.png)

### What is the number of the detected packets?

![37](/Billeder/Uge10-snort/32_Snort the Basics - Exeamples_i.png)

![38](/Billeder/Uge10-snort/34_Snort the Basics - Exeamples_i.png)

Task 7 Using External Rules (MS17-010) 
---------------------------------------

### What is the number of detected packets?

**Comand:**  
 

```text-plain

sudo snort -c <Local rule> -r <pcap> -A full -l .
```

![39](/Billeder/Uge10-snort/21_Snort the Basics - Exeamples_i.png)

### What is the number of detected packets?

**Rule:** 

```text-plain
alert tcp any any -> any 445 (msg: "Exploit Detected!"; flow: to_server, established; content: "IPC$";sid: 20244225; rev:3;)
```

**Comand:**  
 

```text-plain
sudo snort -A full -r <pcap file> -c local rules -l . 

```

![40](/Billeder/Uge10-snort/8_Snort the Basics - Exeamples_i.png)

![41](/Billeder/Uge10-snort/15_Snort the Basics - Exeamples_i.png)

### What is the requested path?

**Rule:** 

```text-plain
alert tcp any any -> any 445 (msg: "Exploit Detected!"; flow: to_server, established; content: "IPC$";sid: 20244225; rev:3;)
```

**Comand:**  
 

```text-plain
sudo snort -r <logs> -X
 
```

![42](/Billeder/Uge10-snort/50_Snort the Basics - Exeamples_i.png)

![43](/Billeder/Uge10-snort/12_Snort the Basics - Exeamples_i.png)

### What is the CVSS v2 score of the MS17-010 vulnerability?

![44](/Billeder/Uge10-snort/49_Snort the Basics - Exeamples_i.png)

Brug google

Task 8 Using External Rules (Log4j) 
------------------------------------

### What is the number of detected packets?

**Comand:**  
 

```text-plain
sudo snort -r <logs> 

```

![45](/Billeder/Uge10-snort/25_Snort the Basics - Exeamples_i.png)

![46](/Billeder/Uge10-snort/17_Snort the Basics - Exeamples_i.png)