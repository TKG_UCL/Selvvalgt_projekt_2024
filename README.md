
**Indholdsfortegnelse**


- [Selvvalgt fordybelse](#selvvalgt-fordybelse)
  - [**Læringsmål\_for\_Selvvalgt\_fordybelse**](#læringsmål_for_selvvalgt_fordybelse)
  - [**Projekt\_planlægning**](#projekt_planlægning)
  - [**Ugelig\_Dokumentation**](#ugelig_dokumentation)


# Selvvalgt fordybelse 

**Læringsmål_for_Selvvalgt_fordybelse**
---------------------------------------

_**Viden**_

_Efter fuldførelse af valgfaget har den studerende viden om:_  
 

*   _Hvad et IDS er._
*   _Hvad et IPS er._
*   _den studerende har viden til hvornår det er godt at benytte IDS og IPS._
*   _Den studerende har viden til at implementere  IDS og IPS i sammen hængene med andre funktioner i et netværk._

_**Færdigheder**_

_Efter fuldførelse af valgfaget kan den studerende:_  
 

*   _Den studerende  kan opsætte IDS_
*   _Den studerende  kan opsætte IPS_

_**Kompetencer**_

_Efter fuldførelse af valgfaget kan den studerende:_  
 

*   _Den studerende kan sammen koble IDS og IPS_
*   _Den studerende kan opstille et fungerende IDS og IPS system_

**Projekt_planlægning**
-----------------------

For at følge med i projektet kan man se hvad den studerende planlægger at lave: 

Underviser bedes kigge under: [https://gitlab.com/tkg_ucl1/projekt-2024/-/boards](https://gitlab.com/tkg_ucl1/projekt-2024/-/boards)    
 

**For at komme til Milestone og danne et overblik hvad den studerende har lavet igennem ungerne kan man følge denne guide:**

**STEP1**

![Billede1](/Billeder/Billede2.png)

* * *


  
**STEP2** 

![Billede2](/Billeder/Billede1.png)

* * *

**STEP3**
Inde i hver uge vil der ligge links til hvad den studerende har benyttet for at søge ny viden.
![Billede3](/Billeder/Billede3.png)


**Ugelig_Dokumentation**
-----------------------

Her vil den studerende skrive om hvad man og lært.

**Uge 8:** ![Uge8_Hvad_IDS_IPS?](Uge 8.md)

**Uge 9:** ![Uge9_Snort_intrduktion](Uge 9.md)

**Uge 10:** ![Uge10-Snort_rules](Uge 10-snort.md)

**Uge 10:** ![Uge10-Firewall](Uge 10-Firewall.md)

**Uge 11 & Uge 12:** ![Uge_11_&_Uge_12_Snot_live_attacks](Uge 11.md)

**Påske**

**Uge 14:** ![Uge14-Logs](Uge 14.md)

**Uge 15:** ![Uge15-IDS-Evasion](Uge 15.md)

**Uge 16:** ![Uge16-Snort-Opsætning](Uge 16.md)