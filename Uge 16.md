# uge 16
Første opsætning (HIDS)
-----------------------

![1](/Billeder/Uge16-SnotOpsætning/30_uge 16_image.png)

### Snort Maskine opsætning

![1](/Billeder/Uge16-SnotOpsætning/uge 16_image.png)

```text-plain
Sudo apt install snort
```

![1](/Billeder/Uge16-SnotOpsætning/1_uge 16_image.png)

```text-plain
Snort --verison
```

![1](/Billeder/Uge16-SnotOpsætning/2_uge 16_image.png)

```text-plain
cd /etc/snort
```

her kopier jeg configuration filen så hvis der bliver lavet fejl kan jeg altid gå tilbage til den normale. 

![1](/Billeder/Uge16-SnotOpsætning/3_uge 16_image.png)

```text-plain
sudo nano /etc/snort/snort.conf
```

Her går man ind op definere hvilket netværk man ønsker at kune sniffe på. 

længere nede i konfigurations filen vil man kunne tilføje eller fjerne regler.

![1](/Billeder/Uge16-SnotOpsætning/4_uge 16_image.png)

```text-plain
Sudo snort -c /etc/snort/snot.conf  -T
```

her compiler man. 

![1](/Billeder/Uge16-SnotOpsætning/6_uge 16_image.png)

i snort kan man lave sin egne regler som er i:

```text-plain
sudo nano /etc/snort/rule/local.rules
```

Her tilføjse den første regl som er at overvåge SSH forbindelser:

```text-plain
regl: alert tcp any any -> any 22 (msg:"beskrivelse"; sid:10000001; rev:1;)
```

Her starter man snort og ber snort om at lytte på den nic som er ens18  

```text-plain
sudo snort -A console -q -c /etc/snort/snort.conf -i ens18
```

![1](/Billeder/Uge16-SnotOpsætning/7_uge 16_image.png)

her har man logget ind på snort maskinen fra kali. 

![1](/Billeder/Uge16-SnotOpsætning/11_uge 16_image.png)

Den næste regl som blev tilføjet er at man vil se om man bliver scannet af nmap. Dette gøre ved at holde øje med SYN flaget, hvis der kommer flere end 5 på 60 sekunder antager snort at man bliver scannet. 

```text-plain
alert tcp any any -> 10.0.0.0/24 any (msg:"Possible Nmap SYN scan detected"; flags:S; threshold: type threshold, track by_src, count 5, seconds 60; sid:1000001;)
```

Efter hver ændring skal man huske at genstarte snort. 

```text-plain
sudo systemctl restart snort
```

  
  
Her bruges kali maskinen til at lave et nmap scan opimod snort maskinen

```text-plain
sudo nmap -sS 10.0.0.11 -vv
```

snort regaere med det samme og laver en masse alerts.

![1](/Billeder/Uge16-SnotOpsætning/12_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/8_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/9_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/10_uge 16_image.png)

  
 

![1](/Billeder/Uge16-SnotOpsætning/13_uge 16_image.png)

Anden opsætning (NIDS/NIPS)
---------------------------

![1](/Billeder/Uge16-SnotOpsætning/31_uge 16_image.png)

Her ses opsætning af netværket. Snort sidder nu på routeren og kan lytte med.

#### Snort Dokumentation

Denne dokumentation er rigtig god har hjulpet meget. 

[https://docs.netgate.com/pfsense/en/latest/packages/snort/index.html#snort](https://docs.netgate.com/pfsense/en/latest/packages/snort/index.html#snort)

### Installation af snort. 

![1](/Billeder/Uge16-SnotOpsætning/15_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/16_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/17_uge 16_image.png)

guide med snort på pf sense

### Opsætning af snort på pf sense

![1](/Billeder/Uge16-SnotOpsætning/18_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/19_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/20_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/21_uge 16_image.png)

![1](/Billeder/Uge16-SnotOpsætning/22_uge 16_image.png)

**Under snort interfaces**

![1](/Billeder/Uge16-SnotOpsætning/23_uge 16_image.png)

**Her kan man Tænde og slukke for regler**

![1](/Billeder/Uge16-SnotOpsætning/24_uge 16_image.png)

**Her vælger man hvor meget sikkerhed man ønsker længere nede kan man selv gøre det hvis man ønsker det**

![1](/Billeder/Uge16-SnotOpsætning/25_uge 16_image.png)

**Vælg en kategori HvIs man tog den**

![1](/Billeder/Uge16-SnotOpsætning/26_uge 16_image.png)

Dette gør at den husker hvilke IP adresser ikke opføre sig pent. 

### Angreb fra LAN og WAN siden.

#### WAN 

![1](/Billeder/Uge16-SnotOpsætning/27_uge 16_image.png)

Scanner fra wan siden

![1](/Billeder/Uge16-SnotOpsætning/28_uge 16_image.png)

Scan fra Wan siden

#### LAN

Samme Nmap scan blev kørt:

```text-plain

sudo nmap -sS 10.0.0.1 -vv
```

![1](/Billeder/Uge16-SnotOpsætning/29_uge 16_image.png)

Scan fra LAN siden