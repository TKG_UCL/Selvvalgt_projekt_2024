# Linkedin learning 
Igennem uge 8 har man opsøgt en guide på linkedin learning og google for at læse og prøve at danne overblik hvad IDS og IPS er!

IDS  IPS er en stor del af defence en depth hvilke man ikke skal overse. der er flere former af IDS og IPS

IDS - HIDS, NIDS
----------------

IDS står for intrusion detection system, og der findes flere typer af dem.

Man kan have en Netværks IDS, altså en NIDS, som sidder på ens netværk. Denne del kunne sidde på en router eller være en server. Den overvåger hele netværket.

HIDS er en type, hvor en IDS sidder på en HOST. Min personlige tanke er, at det minder meget om et antivirusprogram, hvis man skal sammenligne det med noget.

Formålet med en IDS er at observere, hvad der sker på netværket, men den gør ikke nogle aktive tiltag.

![1](/Billeder/Uge8/5_Linkedin learning_image.png)

![2](/Billeder/Uge8/4_Linkedin learning_image.png)

![3](/Billeder/Uge8/1_Linkedin learning_image.png)

IPS
---

IPS står for intrusion prevention system. Der findes også en HIPS, som bare sidder på værten, og en NIPS, som overvåger netværket. 

![4](/Billeder/Uge8/Linkedin learning_image.png)

![5](/Billeder/Uge8/3_Linkedin learning_image.png)

Konklution for Uge 8
--------------------

IDS - er som et overvågnings kamera, som kun opsavere, men ikke går noget.

IPS - er den system system som går noget ud fra et set regler. 

![6](/Billeder/Uge8/2_Linkedin learning_image.png)