**Dette mine egne noter og fremgangsmetode, jeg bygger promært på tryhackme rum for at holde en god struktur har jeg indsætter en kommentar til mig selv samt laver headers så der bliver lavet en indholdesfortegelse i egen note**

# Snort Challenge - Live Attacks
Task 2 Scenario 1 | Brute-Force 
--------------------------------

Her skal man finde ip of port som angriber ens enhed

### Fremgangs metode

her logger vælger man at logge så meget information på computeren. Dette sker ved at bruge denne command:

```text-plain
Sudo snort -dev -l . 
```

* * *

Der efter har man en log fil:

![1](/Billeder/Uge11/3_Snort Challenge - Live Attacks.png)

hvor man så bruger:

```text-plain
 sudo snort -r <logfil navn>
```

* * *

her har jeg siddet og kigget på log filer i en time. og det ligner meget traffik kommer ind på port 80 og port 22 aka ssh

`sudo snort -r <logfil navn>` ‘port 22’

der kom man svar tilbage

![2](/Billeder/Uge11/4_Snort Challenge - Live Attacks.png)

man kan se hvordan der snakkes imellem dem

![3](/Billeder/Uge11/5_Snort Challenge - Live Attacks.png)

* * *

```text-plain
sudo nano /etc/snort/rules/local.rules
```

![4](/Billeder/Uge11/6_Snort Challenge - Live Attacks.png)

```text-plain
sudo snort -c /etc/snort/rules/local.rules -T
```

Reglen compiler lad os se om den virker! 

![5](/Billeder/Uge11/7_Snort Challenge - Live Attacks.png)

glemte lige at tænke hvad inbound og outbound er =) ny regl. min tænke er at blovk angriberen. dog kunne de måske være man skulle bruge <> frem for →   
 

![6](/Billeder/Uge11/8_Snort Challenge - Live Attacks.png)

```text-plain
sudo snort -c /etc/snort/snort.conf -A full
```

okie efter ALT for meget tid har man nu lært at for at sætte IPS mode igang skal man skrive "-Q --daq afpacket" så dette ville se sådan ud:  
 

```text-plain
sudo snort -c /etc/snort/snort.conf -c /etc/snort/snort.conf -Q --daq afpacket -A full
```

reglen blev og til:

```text-plain
drop tcp any 22 <> any any (msg:"ssh found!"; sid:100000001; rev:1;)
```

![7](/Billeder/Uge11/9_Snort Challenge - Live Attacks.png)

* * *

### Stop the attack and get the flag (which will appear on your Desktop)

![8](/Billeder/Uge11/10_Snort Challenge - Live Attacks.png)

### What is the name of the service under attack?

![9](/Billeder/Uge11/11_Snort Challenge - Live Attacks.png)

### What is the used protocol/port in the attack?

![10](/Billeder/Uge11/12_Snort Challenge - Live Attacks.png)

Task 3 Scenario 2 | Reverse-Shell
---------------------------------

Her skal man finde hvilke enheder der har overtaget ens enhed

### Fremgangs metode

her ønsker man at logge information

```text-plain
Sudo snort -dev -l . 
```

![11](/Billeder/Uge11/13_Snort Challenge - Live Attacks.png)

```text-plain
 sudo snort -r <logfil navn>
```

port 4444 springer mig direkte i øjet på mig og jeg tænker netcat =) 

![12](/Billeder/Uge11/14_Snort Challenge - Live Attacks.png)

total med port 4444

![13](/Billeder/Uge11/15_Snort Challenge - Live Attacks.png)

det er helt sikker port 4444 man skal arbejde med

```text-plain
sudo nano /etc/snort/rules/local.rules
```

![14](/Billeder/Uge11/16_Snort Challenge - Live Attacks.png)

```text-plain
sudo snort -c /etc/snort/snort.conf -c /etc/snort/snort.conf -Q --daq afpacket -A full
```

### Stop the attack and get the flag (which will appear on your Desktop)

![15](/Billeder/Uge11/17_Snort Challenge - Live Attacks.png)

### What is the used protocol/port in the attack?

![16](/Billeder/Uge11/18_Snort Challenge - Live Attacks.png)

### Which tool is highly associated with this specific port number?

![17](/Billeder/Uge11/19_Snort Challenge - Live Attacks.png)